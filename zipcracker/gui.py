"""
@author: Mijo Gracanin
"""

import os
import tkinter as tk
from tkinter import ttk, filedialog, messagebox
from threading import Thread
from zipcracker import cracker


class ZipCrackerGUI(tk.Frame):

    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.grid(column=0, row=0, sticky=('N', 'W', 'E', 'S'), padx=(3, 10), pady=(3, 10))
        self.columnconfigure(0, weight=0)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(4, weight=1)
        self.filename = tk.StringVar()  # Path to the file to be cracked
        self.dict_file = tk.StringVar()  # Path to dictionary
        self.is_dict_used = tk.IntVar()
        self.use_small_letters = tk.IntVar()
        self.use_big_letters = tk.IntVar()
        self.use_numbers = tk.IntVar()
        self.use_special_chars = tk.IntVar()
        self.min_len = tk.IntVar()  # min password length
        self.max_len = tk.IntVar()
        self.zip_cracker = cracker.ZipCracker()

        #widgets
        self.file_lbl = ttk.Label(self, text="Zip file")
        self.file_entry = ttk.Entry(self, textvariable=self.filename)
        self.file_btn = ttk.Button(self, text="Browse", command=self.browse_file)
        self.dict_lbl = ttk.Label(self, text="Dictionary")
        self.dict_entry = ttk.Entry(self, textvariable=self.dict_file)
        self.dict_btn = ttk.Button(self, text="Browse", command=self.browse_dict)
        self.dict_chb = ttk.Checkbutton(self, text="Use dictionary", variable=self.is_dict_used)
        self.lbl_frame = ttk.Labelframe(self, text="Brute force settings")
        self.small_letters_chb = ttk.Checkbutton(self.lbl_frame, text="[a-z]", variable=self.use_small_letters)
        self.big_letters_chb = ttk.Checkbutton(self.lbl_frame, text="[A-Z]", variable=self.use_big_letters)
        self.numbers_chb = ttk.Checkbutton(self.lbl_frame, text="[0-9]", variable=self.use_numbers)
        self.special_chars_chb = ttk.Checkbutton(self.lbl_frame, text="[!,#,$,%,&, ...]",
                                                 variable=self.use_special_chars)
        vcmd = (master.register(self.validate), '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.min_len_lbl = ttk.Label(self.lbl_frame, text="Min password length")
        self.min_len_entry = ttk.Entry(self.lbl_frame, width=2, textvariable=self.min_len, validate="key",
                                       validatecommand=vcmd)
        self.max_len_lbl = ttk.Label(self.lbl_frame, text="Max password length")
        self.max_len_entry = ttk.Entry(self.lbl_frame, width=2, textvariable=self.max_len, validate="key",
                                       validatecommand=vcmd)
        self.brute_force_btn = ttk.Button(self, text="Crack zip!", command=self.crack_file)
        self.arrange_widgets()

        self.progress_dlg = None
        self.t = None  # working thread

    def arrange_widgets(self):
        self.file_lbl.grid(row=0, column=0, sticky='W', padx=3, pady=3)
        self.file_entry.grid(row=0, column=1, sticky=('E', 'W'), padx=3, pady=3)
        self.file_btn.grid(row=0, column=2, sticky=('N', 'W'), padx=3, pady=3)

        self.dict_lbl.grid(row=1, column=0, sticky='W', padx=3, pady=3)
        self.dict_entry.grid(row=1, column=1, sticky=('E', 'W'), padx=3, pady=3)
        self.dict_btn.grid(row=1, column=2, sticky=('N', 'W'), padx=3, pady=3)

        self.dict_chb.grid(row=2, column=1, padx=3, pady=3)

        self.lbl_frame.grid(row=3, column=0, columnspan=3, sticky=('E', 'W'), padx=3, pady=3)
        self.lbl_frame.columnconfigure(0, weight=1)
        self.lbl_frame.columnconfigure(1, weight=1)
        self.lbl_frame.columnconfigure(2, weight=1)
        self.lbl_frame.columnconfigure(3, weight=1)
        self.small_letters_chb.grid(row=0, column=0, padx=3, pady=3)
        self.big_letters_chb.grid(row=0, column=1, padx=3, pady=3)
        self.numbers_chb.grid(row=0, column=2, padx=3, pady=3)
        self.special_chars_chb.grid(row=0, column=3, padx=3, pady=3)
        self.min_len_lbl.grid(row=1, column=0, sticky='E', padx=3, pady=3)
        self.min_len_entry.grid(row=1, column=1, sticky='W', padx=3, pady=3)
        self.max_len_lbl.grid(row=1, column=2, sticky='E', padx=3, pady=3)
        self.max_len_entry.grid(row=1, column=3, sticky='W', padx=3, pady=3)

        self.brute_force_btn.grid(row=4, column=0, sticky='S', columnspan=3, padx=3, pady=(12, 3))

    def browse_file(self):
        f = filedialog.askopenfilename(filetypes=(("zip", "*.zip"), ))
        self.filename.set(f)

    def browse_dict(self):
        f = filedialog.askopenfilename(filetypes=(("text", "*.txt"), ))
        self.dict_file.set(f)

    def crack_file(self):
        dict_file = self.dict_file.get()

        if not os.path.isfile(self.filename.get()):
            messagebox.showerror(title='Error', message="Zip file path is not valid")
        elif self.is_dict_used.get() == 1 and not os.path.isfile(dict_file):
            messagebox.showerror(title='Error', message="Dictionary path is not valid")
        else:
            if self.is_dict_used.get() == 0:
                dict_file = None

            options = {
                'small_letters': self.use_small_letters.get(),
                'big_letters': self.use_big_letters.get(),
                'numbers': self.use_numbers.get(),
                'special_chars': self.use_special_chars.get(),
                'min_len': int(self.min_len.get()),
                'max_len': int(self.max_len.get())
            }

            self.progress_dlg = tk.Toplevel(self)
            self.progress_dlg.minsize(120, 100)
            self.progress_dlg.title('Cracking...')
            progressbar = ttk.Progressbar(self.progress_dlg, orient=tk.HORIZONTAL, length=100, mode='indeterminate')
            progressbar.grid(column=0, row=0, sticky=('E', 'W'), padx=10, pady=10)
            progressbar.start()
            cancel_btn = ttk.Button(self.progress_dlg, text="Cancel", command=self.cancel_cracking)
            cancel_btn.grid(column=0, row=1, sticky=('E', 'W'), padx=10, pady=10)
            self.t = Thread(target=self.zip_cracker.crack_file,
                            args=(self.filename.get(), dict_file, self.show_password, options))
            self.t.start()
            self.progress_dlg.grab_set()
            self.progress_dlg.transient(self)
            self.wait_window(self.progress_dlg)

    def cancel_cracking(self):
        self.zip_cracker.stop_running()
        self.t.join()
        self.progress_dlg.destroy()

    def show_password(self):
        self.progress_dlg.destroy()
        messagebox.showinfo(title='Password', message=self.zip_cracker.pwd)

    def set_default_dict(self):
        #TODO
        pass

    def validate(self, action, index, value_if_allowed, prior_value, text, validation_type, trigger_type, widget_name):
        return text.isdigit()


if __name__ == '__main__':
    root = tk.Tk()
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    root.geometry('460x220+30+30')
    root.minsize(width=460, height=220)
    root.title("ZipCracker")
    if "nt" == os.name:
        root.wm_iconbitmap(bitmap="../data/images/app_icon.ico")
    else:
        root.wm_iconbitmap(bitmap="@../data/images/app_icon.xbm")
    mainframe = ZipCrackerGUI(root)
    root.mainloop()