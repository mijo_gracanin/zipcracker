'''
Created on Aug 7, 2013

@author: mijo, referenced
'''


from zipfile import ZipFile, zlib
import itertools


class ZipCracker:

    small_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                     'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z']

    big_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                   'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                   'W', 'X', 'Y', 'Z']

    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    special_chars = ['!', '#', '$', '%', '&', '/', '=', '?', '(', ')', '*',
                     ';', ',', '.', ':', '-', '_']

    def __init__(self):
        self._running = True
        self.pwd = ""

    def stop_running(self):
        self._running = False

    def crack_file(self, file, dictionary=None, callback=None, options=None):
        self._running = True
        zip_file = ZipFile(file)
        if self.is_pwd(zip_file, None):
            self.pwd = None
        else:
            if dictionary:
                self.pwd = self.crack_using_dict(zip_file, dictionary)
            else:
                self.pwd = self.crack_using_brute_force(zip_file, options)
        if callback:
                callback()
        return self.pwd

    @staticmethod
    def is_pwd(zip_file, password=None):
        try:
            if password:
                zip_file.extractall(path='extract', pwd=password.encode(encoding='utf_8', errors='strict'))
            else:
                zip_file.extractall(path='extract')
            return True
        except RuntimeError:
            pass
        except zlib.error:
            pass
        except:
            raise
        return False

    def crack_using_dict(self, zip_file, dictionary):
        d = open(dictionary)
        for line in d.readlines():
            if not self._running:
                return '-canceled-'
            password = line.strip('\n')
            if self.is_pwd(zip_file, password):
                return password
        return None

    def crack_using_brute_force(self, zip_file, options):
        """
        options={small_letters, big_letters, numbers, special_chars, pwd_len}
        """
        chars = []
        if options['small_letters'] == 1:
            chars.extend(ZipCracker.small_letters)
        if options['big_letters'] == 1:
            chars.extend(ZipCracker.big_letters)
        if options['numbers'] == 1:
            chars.extend(ZipCracker.numbers)
        if options['special_chars'] == 1:
            chars.extend(ZipCracker.special_chars)

        for r in range(options['min_len'], options['max_len']):
            for tup in itertools.permutations(chars, r):
                if not self._running:
                    return '-canceled-'
                password = "".join(tup)
                if self.is_pwd(zip_file, password):
                    return password

        return ""
