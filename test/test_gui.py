'''
Created on Sep 14, 2013

@author: mijo
'''
import unittest
import tkinter
import os
from zipcracker import gui


class Test(unittest.TestCase):


    def setUp(self):
        root = tkinter.Tk()
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        root.title("ZipCracker")
        self.mainframe = gui.ZipCrackerGUI(root)


    def tearDown(self):
        pass


    def test_set_default_dict(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()