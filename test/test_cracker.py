'''
Created on Sep 7, 2013

@author: mijo
'''
import unittest
import os
from zipcracker import cracker
from zipfile import ZipFile


class Test(unittest.TestCase):

    def setUp(self):
        self.zcracker = cracker.ZipCracker()
        self.file_area51 = '../data/test_resources/zip_area51.zip'
        self.file_nopass = '../data/test_resources/zip_nopass.zip'
        self.dict = '../data/test_resources/dict.txt'

        if not (os.path.exists('../test_resources') and
                    os.path.isfile(self.dict) and
                    os.path.isfile(self.file_area51) and
                    os.path.isfile(self.file_nopass)):
            raise RuntimeError
        self.zfile_area51 = ZipFile(self.file_area51)

    def tearDown(self):
        pass

    def test_crack_file(self):
        pwd = self.zcracker.crack_file(self.file_area51, self.dict)
        self.assertTrue(pwd == 'area51')
        pwd = self.zcracker.crack_file(self.file_nopass, self.dict)
        self.assert_(pwd is None)

    def test_crack_using_dict(self):
        pwd = self.zcracker.crack_using_dict(self.zfile_area51, self.dict)
        self.assertTrue(pwd == 'area51')

    def test_crack_using_brute_force(self):
        pwd = self.zcracker.crack_using_brute_force(self.zfile_area51)
        self.assertTrue(pwd == 'area51')

    def test_is_valid_pwd(self):
        self.assertTrue(self.zcracker.is_pwd(self.zfile_area51, 'area51'))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()